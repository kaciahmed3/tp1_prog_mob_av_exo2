import 'package:application2/models/question_model.dart';

List<QuestionModel> getQuestions(){

  List<QuestionModel> questions =  [];
  QuestionModel questionModel = QuestionModel(question: '', answer: '');



  //1
  questionModel.setQuestion("Le drapeau français est bleu, blanc et rouge ?");
  questionModel.setAnswer("True");
  questionModel.setImageUrl("https://api-abroad.sfo2.cdn.digitaloceanspaces.com/images/_1200x675_crop_center-center_82_line/france-paris-tour-eiffel-77676271.jpg");
  questions.add(questionModel);
  questionModel =  QuestionModel(question: '', answer: '');

  //2
  questionModel.setQuestion("Bruxelles n’est pas en France ?");
  questionModel.setAnswer("True");
  questionModel.setImageUrl("https://media.routard.com/image/80/0/bruxelles-grand-place.1537800.w630.jpg");
  questions.add(questionModel);

  questionModel =  QuestionModel(question: '', answer: '');

  //3
  questionModel.setQuestion("La capitale de la France est Berlin ?");
  questionModel.setAnswer("False");
  questionModel.setImageUrl("https://www.sortiraparis.com/images/1001/83517/672218-visuel-paris-tour-eiffel-beau-temps.jpg");
  questions.add(questionModel);

  questionModel =  QuestionModel(question: '', answer: '');



  return questions;

}